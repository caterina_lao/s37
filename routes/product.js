const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Retrieve all active the products

router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController =>res.send(resultFromController))
});

// Create Products (Admin Only)

router.post("/add", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	productController.addProduct(userData,req.body).then(resultFromController => res.send(resultFromController))
});

//Retrieve single product

router.get("/:productId", (req, res) => {

	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController))
}); 

// Update Product Information

router.put("/products/:productId", auth.verify,(req, res) => {
	const user = auth.decode(req.headers.authorization);

	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Archive product

router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	productController.archiveProduct(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router;