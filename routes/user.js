const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Setting up User as Admin

router.put("/:userId/setAsAdmin", (req, res) => {
	
	userController.updateUserStatus(req.params, req.body).then (resultFromController => res.send(resultFromController));
});

// transaction

router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : req.body.userId,
		productId: req.body.productId
	
	}

		if(userData.isAdmin == false) {

		userController.order(data).then(resultFromController => res.send(resultFromController));

	} else {

		res.send("You are not Authorized to Enter Data")
	}
});


//Get all orders

router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		userController.getAllOrder().then(resultFromController => res.send(resultFromController))
	} else {
		res.send("You are not Authorized to View Data")
	}
});

// Retrieve User's Orders

router.get("/myOrders", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin == false) {
		userController.getUsersOrder(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("You are not Authorized to View Data")
	}
})




module.exports = router;