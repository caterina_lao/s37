const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Username is required']
	},

	email : {
		type: String,
		required: [ true, 'Email is required'] 
	},

	password : {
		type: String,
		required: [true, 'Password is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
	{
		productId: {
			type:String,
			required: [true, 'Product Id is required']
		},

		totalAmount: {
			type: Number,
			required: false
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		},

	}
	]

});


module.exports = mongoose.model('User', userSchema);