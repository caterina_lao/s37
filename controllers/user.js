const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		name : reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

//User Authentication

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			}
			else {
				return false
			}
		}
	})
}

//Setting up User as Admin

module.exports.updateUserStatus = (reqParams, reqBody) => {

	let updateUserStatus = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(reqParams.userId, updateUserStatus).then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

//Create Order

module.exports.order = async(data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orders.push({productId: data.productId});
			 	
		return user.save().then((user,error) => {
			
			if(error) {
			
				return false
			} else {
				return true
			}
		})
	})
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.customers.push({userId: data.userId});
		return product.save().then((product, error) => {
			
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}
};


//Get all orders

module.exports.getAllOrder =() => {

	
	return Product.find({}, {customers: 1}).then(result =>{
		return result;
	})
};

// Retrieve User's Orders

module.exports.getUsersOrder = (data) => {

	return User.find({}, {orders: 1}).then(result => {
		return result;
	})
};
