const Product = require("../models/Product");

// Retrieve all Active Products

module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

//Create Product (Admin Only)

module.exports.addProduct = async (user,reqBody) => {
	if(user.isAdmin == true){
		let newProduct = new Product ({

			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price

		})

		return newProduct.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	}  else {
		return ("You are not Authorized to Enter Data");
	}
};

//Retrieve single product

module.exports.getSpecificProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		let getProduct = {
			name : result.name,
			description: result.description,
			price: result.price
		} 
		return getProduct
	});
};

// Update Product Information

module.exports.updateProduct = async(user,reqParams, reqBody) => {

	if(user.isAdmin == true) {

		let updateActiveField = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then ((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return("You are not Authorized to Enter Data")
	}
};

// Archive Product

module.exports.archiveProduct =  async(user, reqParams, reqBody) => {
	if(user.isAdmin == true) {

		let updateActiveField = {
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return ("You are not Authorized to Enter Data")
	}
};

